"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyManager = void 0;
const verifyManager = (req, res, next) => {
    if (req.role !== "Manager")
        return res.status(403).json("Access denied");
    next();
};
exports.verifyManager = verifyManager;
//# sourceMappingURL=verifyManager.js.map