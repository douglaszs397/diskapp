"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyClient = exports.verifyManager = void 0;
const verifyManager = (req, res, next) => {
    if (req.role !== "Manager")
        return res.status(403).json("Access denied");
    next();
};
exports.verifyManager = verifyManager;
const verifyClient = (req, res, next) => {
    if (req.role !== "Client")
        return res.status(403).json("Access denied");
    next();
};
exports.verifyClient = verifyClient;
//# sourceMappingURL=verifyRole.js.map