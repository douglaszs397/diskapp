"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("./database/database");
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const app = (0, express_1.default)();
const auth_1 = __importDefault(require("./routes/auth"));
const disk_1 = __importDefault(require("./routes/disk"));
database_1.db.authenticate()
    .then(() => console.log("Database connected..."))
    .catch((err) => console.log(err));
// settings
app.set('port', 3000);
// middlewares
app.use((0, morgan_1.default)('dev'));
app.use(express_1.default.json());
// routes
app.use("/api/auth", auth_1.default);
app.use("/api/disk", disk_1.default);
exports.default = app;
//# sourceMappingURL=app.js.map