"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const verifyToken_1 = require("../middlewares/verifyToken");
const disk_controller_1 = require("../controllers/disk.controller");
const verifyRole_1 = require("../middlewares/verifyRole");
const router = (0, express_1.Router)();
// FREE ROUTES
router.get('/list', disk_controller_1.listDisks); // token free
router.get('/detail/:idDisk', disk_controller_1.detailDisk); // // token free
// MANAGER ROUTES
router.post('/create', verifyToken_1.tokenValidation, verifyRole_1.verifyManager, disk_controller_1.createDisk); // token manager
/* router.post('/uploadCover', verifyManager, tokenValidation, uploadCover); */
router.patch('/update/:idDisk', verifyToken_1.tokenValidation, verifyRole_1.verifyManager, disk_controller_1.updateDisk); // token manager
router.post('/delete/:idDisk', verifyToken_1.tokenValidation, verifyRole_1.verifyManager, disk_controller_1.deleteDisk); // token manager
router.post('/disable/:idDisk', verifyToken_1.tokenValidation, verifyRole_1.verifyManager, disk_controller_1.disableDisk); // token manager
// CLIENT ROUTES
router.post('/likeDisk/:idDisk', verifyToken_1.tokenValidation, verifyRole_1.verifyClient, disk_controller_1.likeDisk); // token client
exports.default = router;
//# sourceMappingURL=disk.js.map