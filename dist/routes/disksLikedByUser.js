"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const verifyToken_1 = require("../middlewares/verifyToken");
const disksLikedByUser_controller_1 = require("../controllers/disksLikedByUser.controller");
const router = (0, express_1.Router)();
router.post('/likeDisk/:idDisk', verifyToken_1.tokenValidation, disksLikedByUser_controller_1.likeDisk);
exports.default = router;
//# sourceMappingURL=disksLikedByUser.js.map