"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database/database");
const sequelize_1 = require("sequelize");
const Disk = database_1.db.define("disk", {
    idDisk: {
        primaryKey: true,
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true
    },
    name: {
        type: sequelize_1.DataTypes.STRING
    },
    genre: {
        type: sequelize_1.DataTypes.STRING
    },
    subgenre: {
        type: sequelize_1.DataTypes.STRING
    },
    year: {
        type: sequelize_1.DataTypes.INTEGER
    },
    band: {
        type: sequelize_1.DataTypes.STRING
    },
    cover: {
        type: sequelize_1.DataTypes.STRING
    },
    price: {
        type: sequelize_1.DataTypes.DECIMAL
    },
    stock: {
        type: sequelize_1.DataTypes.INTEGER
    },
    active: {
        type: sequelize_1.DataTypes.TINYINT
    },
    state: {
        type: sequelize_1.DataTypes.TINYINT
    },
    createdAt: {
        type: sequelize_1.DataTypes.DATE
    },
    updatedAt: {
        type: sequelize_1.DataTypes.DATE
    }
});
exports.default = Disk;
//# sourceMappingURL=Disk.js.map