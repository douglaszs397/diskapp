"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database/database");
const sequelize_1 = require("sequelize");
const Disk_1 = __importDefault(require("./Disk"));
const User_1 = __importDefault(require("./User"));
const DisksLikedByUser = database_1.db.define("disksLikedByUser", {
    idDisksLikedByUser: {
        primaryKey: true,
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true
    },
    idDisk: {
        type: sequelize_1.DataTypes.INTEGER,
        references: {
            model: Disk_1.default,
            key: 'idDisk'
        }
    },
    idUser: {
        type: sequelize_1.DataTypes.INTEGER,
        references: {
            model: User_1.default,
            key: 'idUser'
        }
    },
    likeState: {
        type: sequelize_1.DataTypes.TINYINT
    },
    state: {
        type: sequelize_1.DataTypes.TINYINT
    },
    createdAt: {
        type: sequelize_1.DataTypes.DATE
    },
    updatedAt: {
        type: sequelize_1.DataTypes.DATE
    }
});
exports.default = DisksLikedByUser;
//# sourceMappingURL=DisksLikedByUser.js.map