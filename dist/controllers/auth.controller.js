"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.restorePassword = exports.signin = exports.signup = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const User_1 = __importStar(require("../models/User"));
const User_2 = require("../models/User");
const signup = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { role, username, email, password, name, lastname, securityQuestion } = req.body;
        const newUser = User_1.default.build();
        newUser.role = role;
        newUser.username = username;
        newUser.email = email;
        newUser.password = yield (0, User_2.encryptPassword)(password);
        newUser.name = name;
        newUser.lastname = lastname;
        newUser.securityQuestion = securityQuestion;
        newUser.state = true;
        const savedUser = yield newUser.save();
        // jwt
        const token = jsonwebtoken_1.default.sign({ id: savedUser.idUser, role: savedUser.role, email: savedUser.email }, process.env.TOKEN_SECRET || 'testtoken');
        res.header('auth-token', token).json(savedUser);
    }
    catch (error) {
        throw error;
    }
});
exports.signup = signup;
const signin = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { email, password } = req.body;
        const userFound = yield User_1.default.findOne({ where: { email: email } });
        if (!userFound)
            return res.status(403).json("Invalid email");
        const checkPass = yield (0, User_1.validatePassword)(password, userFound.password);
        if (!checkPass)
            return res.status(403).json("Invalid password");
        const token = jsonwebtoken_1.default.sign({ id: userFound.idUser, role: userFound.role, email: userFound.email }, process.env.TOKEN_SECRET || 'testtoken', {
            expiresIn: 60 * 60 * 24
        });
        return res.header('auth-token', token).json(userFound);
    }
    catch (error) {
        throw error;
    }
});
exports.signin = signin;
const restorePassword = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { email, newPassword, repeatedPassword, securityQuestion } = req.body;
        const userFound = yield User_1.default.findOne({ where: { email: email } });
        if (!userFound)
            return res.status(403).json("User doesn't exist");
        const checkSecurityQuestion = yield User_1.default.findOne({ where: { email: email, securityQuestion: securityQuestion } });
        if (!checkSecurityQuestion)
            return res.status(403).json("Wrong security question value");
        if (newPassword !== repeatedPassword)
            return res.status(403).json("Passwords doesn't match");
        userFound.password = yield (0, User_2.encryptPassword)(newPassword);
        const userUpdated = yield userFound.save();
        return res.json(userUpdated);
    }
    catch (error) {
        throw error;
    }
});
exports.restorePassword = restorePassword;
//# sourceMappingURL=auth.controller.js.map