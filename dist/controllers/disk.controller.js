"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.likeDisk = exports.disableDisk = exports.deleteDisk = exports.updateDisk = exports.uploadCover = exports.createDisk = exports.detailDisk = exports.listDisks = void 0;
const DisksLikedByUser_1 = __importDefault(require("../models/DisksLikedByUser"));
const Disk_1 = __importDefault(require("../models/Disk"));
// LIST
const listDisks = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const filter = { where: { state: true } };
        const disks = yield Disk_1.default.findAll(filter);
        return res.json(disks);
    }
    catch (error) {
        throw error;
    }
});
exports.listDisks = listDisks;
// DETAILS
const detailDisk = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { idDisk } = req.params;
        const filter = { where: { state: true, idDisk } };
        const diskFound = yield Disk_1.default.findOne(filter);
        if (!diskFound)
            return res.status(400).json("Invalid disk");
        return res.json(diskFound);
    }
    catch (error) {
        throw error;
    }
});
exports.detailDisk = detailDisk;
// CREATE
const createDisk = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (req.role !== "Manager")
            return res.status(403).json("Access denied");
        const { name, genre, subgenre, year, band, cover, price, stock } = req.body;
        const newDisk = Disk_1.default.build();
        newDisk.name = name;
        newDisk.genre = genre;
        newDisk.subgenre = subgenre;
        newDisk.year = year;
        newDisk.band = band;
        newDisk.cover = cover;
        newDisk.price = price;
        newDisk.stock = stock;
        newDisk.active = true;
        newDisk.state = true;
        const savedDisk = yield newDisk.save();
        return res.json(savedDisk);
    }
    catch (error) {
        throw error;
    }
});
exports.createDisk = createDisk;
// UPLOAD
const uploadCover = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
});
exports.uploadCover = uploadCover;
// UPDATE
const updateDisk = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (req.role !== "Manager")
            return res.status(403).json("Access denied");
        const { name, genre, subgenre, year, band, price, stock } = req.body;
        const { idDisk } = req.params;
        const filter = { where: { state: true, idDisk } };
        const diskFound = yield Disk_1.default.findOne(filter);
        if (!diskFound)
            return res.status(400).json("Invalid disk");
        diskFound.name = name;
        diskFound.genre = genre;
        diskFound.subgenre = subgenre;
        diskFound.year = year;
        diskFound.band = band;
        diskFound.price = price;
        diskFound.stock = stock;
        const diskUpdated = yield diskFound.save();
        return res.json(diskUpdated);
    }
    catch (error) {
        throw error;
    }
});
exports.updateDisk = updateDisk;
// DELETE
const deleteDisk = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { idDisk } = req.params;
        const filter = { where: { state: true, idDisk } };
        const diskFound = yield Disk_1.default.findOne(filter);
        if (!diskFound)
            return res.status(400).json("Invalid disk");
        diskFound.state = false;
        const diskDeleted = yield diskFound.save();
        return res.json(diskDeleted);
    }
    catch (error) {
        throw error;
    }
});
exports.deleteDisk = deleteDisk;
// DISABLE
const disableDisk = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { idDisk } = req.params;
        const filter = { where: { state: true, idDisk } };
        const diskFound = yield Disk_1.default.findOne(filter);
        if (!diskFound)
            return res.status(400).json("Invalid disk");
        diskFound.active = false;
        const diskDisabled = yield diskFound.save();
        return res.json(diskDisabled);
    }
    catch (error) {
        throw error;
    }
});
exports.disableDisk = disableDisk;
// LIKE
const likeDisk = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { idDisk } = req.params;
        const filter = { where: { state: true, idDisk } };
        const diskFound = yield Disk_1.default.findOne(filter);
        if (!diskFound)
            return res.status(400).json("Invalid disk");
        // find another same register and set states to false
        const filter2 = { where: { state: true, idDisk: diskFound.idDisk, idUser: req.idUser } };
        const diskLiked = yield DisksLikedByUser_1.default.findOne(filter2);
        if (diskLiked) {
            diskLiked.likeState = false;
            diskLiked.state = false;
            yield diskLiked.save();
        }
        const newDiskLikedByUser = DisksLikedByUser_1.default.build();
        newDiskLikedByUser.idDisk = parseInt(idDisk);
        newDiskLikedByUser.idUser = req.idUser;
        newDiskLikedByUser.likeState = true;
        newDiskLikedByUser.state = true;
        const diskLikedSaved = yield newDiskLikedByUser.save();
        return res.json(diskLikedSaved);
    }
    catch (error) {
        throw error;
    }
});
exports.likeDisk = likeDisk;
//# sourceMappingURL=disk.controller.js.map