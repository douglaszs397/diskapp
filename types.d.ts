declare namespace Express {
  export interface Request {
    idUser: number;
    role: string;
    email: string;
  }
}