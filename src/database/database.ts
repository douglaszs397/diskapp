import { Sequelize } from "sequelize";

export const db = new Sequelize('DiskDB', 'postgres', 'password', {
  host: 'localhost',
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  define: {
    freezeTableName: true
  },
  logging: true
});