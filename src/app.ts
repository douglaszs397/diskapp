import { db } from './database/database';
import express, { Application } from 'express';
import morgan from 'morgan';

const app: Application = express();

import authRoutes from './routes/auth';
import diskRoutes from './routes/disk';


db.authenticate()
  .then(() => console.log("Database connected..."))
  .catch((err: any) => console.log(err))

// settings
app.set('port', 3000);

// middlewares
app.use(morgan('dev'));
app.use(express.json());

// routes
app.use("/api/auth", authRoutes);
app.use("/api/disk", diskRoutes);


export default app;