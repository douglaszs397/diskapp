import { db } from '../database/database';
import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import User, { validatePassword } from '../models/User';
import { encryptPassword } from '../models/User';


export const signup = async (req: Request, res: Response) => {

  try {

    const { role, username, email, password, name, lastname, securityQuestion } = req.body;

    const newUser = User.build();

    newUser.role = role;
    newUser.username = username;
    newUser.email = email;
    newUser.password = await encryptPassword(password);
    newUser.name = name;
    newUser.lastname = lastname;
    newUser.securityQuestion = securityQuestion;
    newUser.state = true;

    const savedUser = await newUser.save();

    // jwt
    const token: string = jwt.sign({ id: savedUser.idUser, role: savedUser.role, email: savedUser.email }, process.env.TOKEN_SECRET || 'testtoken');

    res.header('auth-token', token).json(savedUser);

  } catch (error) {

    throw error;

  }

};


export const signin = async (req: Request, res: Response) => {

  try {

    const { email, password } = req.body;

    const userFound = await User.findOne({ where: { email: email } });
    if (!userFound) return res.status(403).json("Invalid email");

    const checkPass = await validatePassword(password, userFound.password);
    if (!checkPass) return res.status(403).json("Invalid password")

    const token: string = jwt.sign({ id: userFound.idUser, role: userFound.role, email: userFound.email }, process.env.TOKEN_SECRET || 'testtoken', {
      expiresIn: 60 * 60 * 24
    });

    return res.header('auth-token', token).json(userFound);

  } catch (error) {

    throw error;

  }

};

export const restorePassword = async (req: Request, res: Response) => {

  try {

    const { email, newPassword, repeatedPassword, securityQuestion } = req.body;

    const userFound = await User.findOne({ where: { email: email } });
    if (!userFound) return res.status(403).json("User doesn't exist");

    const checkSecurityQuestion = await User.findOne({ where: { email: email, securityQuestion: securityQuestion } });
    if (!checkSecurityQuestion) return res.status(403).json("Wrong security question value");

    if (newPassword !== repeatedPassword) return res.status(403).json("Passwords doesn't match");

    userFound.password = await encryptPassword(newPassword);

    const userUpdated = await userFound.save();

    return res.json(userUpdated);

  } catch (error) {

    throw error;

  }

};