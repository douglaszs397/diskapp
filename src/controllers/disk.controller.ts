import { Request, Response } from 'express';
import DisksLikedByUser from '../models/DisksLikedByUser';
import Disk from '../models/Disk';

// LIST
export const listDisks = async (req: Request, res: Response) => {

  try {

    const filter = { where: { state: true } };
    const disks = await Disk.findAll(filter);

    return res.json(disks);

  } catch (error) {

    throw error;
    
  }

};

// DETAILS
export const detailDisk = async (req: Request, res: Response) => {

  try {

    const { idDisk } = req.params;

    const filter = { where: { state: true, idDisk } };
    const diskFound = await Disk.findOne(filter);
    if (!diskFound) return res.status(400).json("Invalid disk");

    return res.json(diskFound);

  } catch (error) {

    throw error;

  }

}

// CREATE
export const createDisk = async (req: Request, res: Response) => {

  try {

    if (req.role !== "Manager") return res.status(403).json("Access denied");

    const { name, genre, subgenre, year, band, cover, price, stock } = req.body;

    const newDisk = Disk.build();

    newDisk.name = name;
    newDisk.genre = genre;
    newDisk.subgenre = subgenre;
    newDisk.year = year;
    newDisk.band = band;
    newDisk.cover = cover;
    newDisk.price = price;
    newDisk.stock = stock;
    newDisk.active = true;
    newDisk.state = true;

    const savedDisk = await newDisk.save();

    return res.json(savedDisk);

  } catch (error) {

    throw error;

  }
};

// UPLOAD
export const uploadCover = async (req: Request, res: Response) => {

};

// UPDATE
export const updateDisk = async (req: Request, res: Response) => {

  try {

    if (req.role !== "Manager") return res.status(403).json("Access denied");

    const { name, genre, subgenre, year, band, price, stock } = req.body;
    const { idDisk } = req.params;

    const filter = { where: { state: true, idDisk } };
    const diskFound = await Disk.findOne(filter);
    if (!diskFound) return res.status(400).json("Invalid disk");

    diskFound.name = name;
    diskFound.genre = genre;
    diskFound.subgenre = subgenre;
    diskFound.year = year;
    diskFound.band = band;
    diskFound.price = price;
    diskFound.stock = stock;

    const diskUpdated = await diskFound.save();

    return res.json(diskUpdated);

  } catch (error) {

    throw error;

  }

};

// DELETE
export const deleteDisk = async (req: Request, res: Response) => {

  try {

    const { idDisk } = req.params;

    const filter = { where: { state: true, idDisk } };
    const diskFound = await Disk.findOne(filter);
    if (!diskFound) return res.status(400).json("Invalid disk");

    diskFound.state = false;

    const diskDeleted = await diskFound.save();

    return res.json(diskDeleted);

  } catch (error) {

    throw error;

  }

};

// DISABLE
export const disableDisk = async (req: Request, res: Response) => {

  try {

    const { idDisk } = req.params;

    const filter = { where: { state: true, idDisk } };
    const diskFound = await Disk.findOne(filter);
    if (!diskFound) return res.status(400).json("Invalid disk");

    diskFound.active = false;

    const diskDisabled = await diskFound.save();

    return res.json(diskDisabled);

  } catch (error) {

    throw error;

  }

};

// LIKE
export const likeDisk = async (req: Request, res: Response) => {

  try {

    const { idDisk } = req.params;

    const filter = { where: { state: true, idDisk } };
    const diskFound = await Disk.findOne(filter);
    if (!diskFound) return res.status(400).json("Invalid disk");

    // find another same register and set states to false
    const filter2 = { where: { state: true, idDisk: diskFound.idDisk, idUser: req.idUser } };
    const diskLiked = await DisksLikedByUser.findOne(filter2);
    if (diskLiked) {
      diskLiked.likeState = false;
      diskLiked.state = false;
      await diskLiked.save();
    }

    const newDiskLikedByUser = DisksLikedByUser.build();

    newDiskLikedByUser.idDisk = parseInt(idDisk);
    newDiskLikedByUser.idUser = req.idUser;
    newDiskLikedByUser.likeState = true;
    newDiskLikedByUser.state = true;

    const diskLikedSaved = await newDiskLikedByUser.save();

    return res.json(diskLikedSaved);

  } catch (error) {

    throw error;

  }

};

