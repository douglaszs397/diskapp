import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken';

interface IPayload {
  id: number;
  role: string;
  email: string;
  iat: number;
  exp: number;
}

export const tokenValidation = (req: Request, res: Response, next: NextFunction) => {
  const token = req.header("auth-token");

  if (!token) return res.status(401).json('Acces denied');

  const payload = jwt.verify(token, process.env.TOKEN_SECRET || 'testtoken') as IPayload;

  req.idUser = payload.id;
  req.role = payload.role;
  req.email = payload.email;

  next();
};