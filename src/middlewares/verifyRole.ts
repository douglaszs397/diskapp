import { Request, Response, NextFunction } from 'express'

export const verifyManager = (req: Request, res: Response, next: NextFunction) => {

  if (req.role !== "Manager") return res.status(403).json("Access denied");

  next();
}

export const verifyClient = (req: Request, res: Response, next: NextFunction) => {

  if (req.role !== "Client") return res.status(403).json("Access denied");

  next();
}