import { db } from '../database/database';
import { DataTypes, Model } from 'sequelize';

export interface IDisk {
  idDisk?: number;
  name: string;
  genre: string;
  subgenre: string;
  year: number;
  band: string;
  cover: string;
  price: number;
  stock: number;
  active: boolean;
  state?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

interface DiskInstance extends Model<IDisk, any>, IDisk { }

const Disk = db.define<DiskInstance>("disk", {
  idDisk: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true
  },
  name: {
    type: DataTypes.STRING
  },
  genre: {
    type: DataTypes.STRING
  },
  subgenre: {
    type: DataTypes.STRING
  },
  year: {
    type: DataTypes.INTEGER
  },
  band: {
    type: DataTypes.STRING
  },
  cover: {
    type: DataTypes.STRING
  },
  price: {
    type: DataTypes.DECIMAL
  },
  stock: {
    type: DataTypes.INTEGER
  },
  active: {
    type: DataTypes.TINYINT
  },
  state: {
    type: DataTypes.TINYINT
  },
  createdAt: {
    type: DataTypes.DATE
  },
  updatedAt: {
    type: DataTypes.DATE
  }
});

export default Disk;