import { db } from '../database/database';
import { DataTypes, Model } from 'sequelize';
import Disk from './Disk';
import User from './User';

export interface IDisksLikedByUser {
  idDisksLikedByUser?: number;
  idDisk: number;
  idUser: number;
  likeState: boolean;
  state?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

interface DisksLikedByUserInstance extends Model<IDisksLikedByUser, any>, IDisksLikedByUser { }

const DisksLikedByUser = db.define<DisksLikedByUserInstance>("disksLikedByUser", {
  idDisksLikedByUser: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true
  },
  idDisk: {
    type: DataTypes.INTEGER,
    references: {
      model: Disk,
      key: 'idDisk'
    }
  },
  idUser: {
    type: DataTypes.INTEGER,
    references: {
      model: User,
      key: 'idUser'
    }
  },
  likeState: {
    type: DataTypes.TINYINT
  },
  state: {
    type: DataTypes.TINYINT
  },
  createdAt: {
    type: DataTypes.DATE
  },
  updatedAt: {
    type: DataTypes.DATE
  }
});

export default DisksLikedByUser;