import { db } from '../database/database';
import bcrypt from 'bcryptjs';
import { DataTypes, Model } from 'sequelize';

export interface IUser {
  idUser?: number;
  role: string;
  username: string;
  email: string;
  password: string;
  name: string;
  lastname: string;
  securityQuestion: string;
  state?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

interface UserInstance extends Model<IUser, any>, IUser { }

const User = db.define<UserInstance>("user", {
  idUser: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true
  },
  role: {
    type: DataTypes.STRING
  },
  username: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING
  },
  password: {
    type: DataTypes.STRING
  },
  name: {
    type: DataTypes.STRING
  },
  lastname: {
    type: DataTypes.STRING
  },
  securityQuestion: {
    type: DataTypes.STRING
  },
  state: {
    type: DataTypes.TINYINT
  },
  createdAt: {
    type: DataTypes.DATE
  },
  updatedAt: {
    type: DataTypes.DATE
  }
});

export const encryptPassword = async (password: string): Promise<string> => {
  const salt = await bcrypt.genSalt(10);
  return bcrypt.hash(password, salt);
}

export const validatePassword = async (passwordBody: string, passwordUser: string): Promise<boolean> => {
  return await bcrypt.compare(passwordBody, passwordUser);
}

export default User;

