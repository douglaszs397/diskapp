import { Router } from 'express';
import { restorePassword, signin, signup } from '../controllers/auth.controller';

const router: Router = Router();

router.post('/signup', signup);
router.post('/signin', signin);
router.post('/restorePassword', restorePassword);


export default router;