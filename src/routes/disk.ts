import { Router } from 'express';
import { tokenValidation } from '../middlewares/verifyToken';
import { listDisks, createDisk, updateDisk, deleteDisk, disableDisk, uploadCover, likeDisk, detailDisk } from '../controllers/disk.controller';
import { verifyClient, verifyManager } from '../middlewares/verifyRole';

const router: Router = Router();

// FREE ROUTES
router.get('/list', listDisks); // token free
router.get('/detail/:idDisk', detailDisk); // // token free

// MANAGER ROUTES
router.post('/create', tokenValidation, verifyManager, createDisk); // token manager
/* router.post('/uploadCover', verifyManager, tokenValidation, uploadCover); */
router.patch('/update/:idDisk', tokenValidation, verifyManager, updateDisk); // token manager
router.post('/delete/:idDisk', tokenValidation, verifyManager, deleteDisk); // token manager
router.post('/disable/:idDisk', tokenValidation, verifyManager, disableDisk); // token manager

// CLIENT ROUTES
router.post('/likeDisk/:idDisk', tokenValidation, verifyClient, likeDisk); // token client

export default router;